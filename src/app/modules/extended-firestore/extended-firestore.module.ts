import {NgModule} from '@angular/core';
import {FirestoreService} from './firestore.service';

@NgModule({
  providers: [
      FirestoreService
  ]
})
export class ExtendedFirestoreModule { }
