import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import CollectionReference = firebase.firestore.CollectionReference;
import {ReplaySubject} from 'rxjs';
import {Filter} from '../models/filter';
import Query = firebase.firestore.Query;
import DocumentReference = firebase.firestore.DocumentReference;
import {AngularFirestore} from 'angularfire2/firestore';
import {FirestoreDocResult} from '../models/FirestoreDocResult';

@Injectable()
export class FirestoreService {

  constructor(
      private firestore: AngularFirestore
  ) { }

  collection(path: string | CollectionReference | Query, filt?: Filter): ReplaySubject<FirestoreDocResult[]> {
      const observable: ReplaySubject<FirestoreDocResult[]> = new ReplaySubject();

      if (typeof path === 'string') {
          path = this.firestore.collection(path).ref;
      }

      path.onSnapshot(collection => {
          const newData: FirestoreDocResult[] = [];
          const docs = collection.docs;

          for (let i = 0; i < docs.length; i++) {
              const doc = docs[i];
              const docData = doc.data();
              docData.ref = {
                  id: doc.ref.id,
                  path: doc.ref.path
              };
              let isFiltered = true;
              if (filt !== undefined) {
                  isFiltered = this.filter(docData, filt);
              }
              if (isFiltered) {
                  newData.push({
                      data: docData,
                      ref: doc.ref
                  });
              }
          }

          observable.next(newData);
      });

      return observable;
  }

  doc(path: string | DocumentReference): ReplaySubject<FirestoreDocResult> {
      const observable: ReplaySubject<FirestoreDocResult> = new ReplaySubject();
      if (!(path instanceof DocumentReference)) {
          path = this.firestore.doc(path).ref;
      }
      path.onSnapshot(doc => {
          const docData = doc.data();
          docData.ref = doc.ref.id;
          observable.next({
              data: docData,
              ref: doc.ref
          });
      });

      return observable;
  }

  filter(object, filt: Filter) {
      const objectValue = object[filt.key];

      if (filt.operator === '<') {
          return objectValue < filt.value;
      } else if (filt.operator === '<=') {
          return objectValue <= filt.value;
      } else if (filt.operator === '==') {
          return objectValue === filt.value;
      } else if (filt.operator === '!=') {
          return objectValue !== filt.value;
      } else if (filt.operator === '>=') {
          return object.value >= filt.value;
      } else if (filt.operator === '>') {
          return objectValue > filt.value;
      } else if (filt.operator === 'IN') {
          return filt.value.indexOf(objectValue) !== -1;
      } else {
          return false;
      }
  }

}
