export class Filter {
    key: string;
    operator: '<' | '<=' | '==' | '!=' | '>=' | '>' | 'IN';
    value: any;
}
