import {FirestoreRef} from './FirestoreRef';

export class FirestoreDocResult {
    data: any;
    ref: FirestoreRef;
}
